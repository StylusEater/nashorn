/* 
 * Java Nashorn examples taken from:
 * 
 *     http://winterbe.com/posts/2014/04/05/java8-nashorn-tutorial/
 *     
 *     Runme like so ... jjs -cp dist/nashorn.jar js/JavaInJS.js
 */

/* Import our Java class we want to call functions from. */
/* global Java, java */
var JavaInJS = Java.type('com.nashorn.JavaInJS');


/* Run the printName(name) method we defined in Java */
var printNameResponse = JavaInJS.printName('Adam M Dutko');
print(printNameResponse);


/* Run the printClassTypeOfMethodParameter(some_value) method we defined in Java */
/*
 * NOTE: classes from jdk.nashorn.internal are subjec to change
 * 
 *       ... anything internal will change from underneath you ...
 *       
 *       classes from jdk.nashorn.api are INTENDED to be used by developers
 */
// class java.lang.Integer
var printParameterTypeResponseInt = JavaInJS.printClassTypeOfMethodParameter(123);
// class java.lang.Double
var printParameterTypeResponseDouble = JavaInJS.printClassTypeOfMethodParameter(123.00);
// class java.lang.Boolean
var printParameterTypeResponseBoolean = JavaInJS.printClassTypeOfMethodParameter(false);
// class java.lang.String
var printParameterTypeResponseString = JavaInJS.printClassTypeOfMethodParameter("Hello, there!");
// class jdk.nashorn.internal.objects.NativeNumber
var printParameterTypeResponseNumber = JavaInJS.printClassTypeOfMethodParameter(new Number(23));
// class jdk.nashorn.internal.objects.NativeDate
var printParameterTypeResponseDate = JavaInJS.printClassTypeOfMethodParameter(new Date());
// class jdk.nashorn.internal.objects.NativeRegExp
var printParameterTypeResponseRegex = JavaInJS.printClassTypeOfMethodParameter(new RegExp());
// class jdk.nashorn.internal.objects.JO4
var printParameterTypeResponseJSON = JavaInJS.printClassTypeOfMethodParameter({Adam: 'Dutko'});


/*
 *  Use the ScriptObjectMirror class that gives us a Java representation of the 
 * JavaScript object.
 * 
 */
JavaInJS.mirrorObject(
        {
            first_name: 'Adam',
            middle_name: 'Michael',
            last_name: 'Dutko'
        });
        

/*
 * Define a JavaScript object and a method we'll call from Java.
 */
function Person(firstName,middleName,lastName)
{
    this.firstName = firstName;
    this.middleName = middleName;
    this.lastName = lastName;
    
    this.printFullName = function() {
        return this.firstName + " " + this.middleName + " " + this.lastName;
    };
}

var adam = new Person("Adam","Michael","Dutko");
JavaInJS.printFullName(adam);


/*
 * Nashorn let's us have typed Arrays, something JavaScript does not support.
 */
var IntArray = Java.type("int[]");

var array = new IntArray(5);
array[0] = 5;
array[1] = 4;
array[2] = 3;
array[3] = 2;
array[4] = 1;

// We can even catch out of bounds errors!
try {
    array[5] = 23;
} catch (e) {
    //Array index out of range: 5
    print(e.message);
}

// Replace a value by index
array[0] = "17";
print(array[0]);

// Replace a value with the wrong type and get a 0 because String converted to int
array[0] = "wrong type";
print(array[0]);

// Double coerced into Int with loss of precision
array[0] = "17.3";
print(array[0]);


/* 
 * Nashorn also gives us access to the Collections Framework ... let's look
 * at an ArrayList first ...
 */
var ArrayList = Java.type('java.util.ArrayList');
var list = new ArrayList();
list.add('a');
list.add('b');
list.add('c');

// Nashorn uses `for each` to loop over arrays and collections ... it works just
// like the Java foreach ...
for each (var el in list) print(el);

// Another example using for each and a HashMap and direct instantiation ... 
// we don't go through Java.type()
var map = new java.util.HashMap();
map.put('FirstPerson', 'Adam Michael Dutko');
map.put('SecondPerson', 'Chris Daniels');

for each (var e in map.keySet()) print(e);

for each (var e in map.values()) print(e);


/*
 * Nashorn also supports Lambdas and Streams
 */
var list2 = new java.util.ArrayList();
list2.add("ddd2");
list2.add("aaa2");
list2.add("bbb1");
list2.add("aaa1");
list2.add("bbb3");
list2.add("ccc");
list2.add("bbb2");
list2.add("ddd1");

list2
    .stream()
    .filter(function(el) {
        return el.startsWith("aaa");
    })
    .sorted()
    .forEach(function(el) {
        print(el);
    });
    
    
/* 
 * Nashorn helps you to extend Java classes through JavaScript...
 * 
 * ...it also supports Threads!
 */
var Runnable = Java.type('java.lang.Runnable');
var Printer = Java.extend(Runnable, {
    run: function() {
        print('printed from a separate thread');
    }
});

var Thread = Java.type('java.lang.Thread');
new Thread(new Printer()).start();

new Thread(function() {
    print('printed from another thread');
}).start();


/*
 * Parameter overloading
 */


/*
 * Java Beans
 */


/*
 * Function Literals
 */


/*
 * Binding Properties
 */


/*
 * Trimming Strings
 */


/*
 * Whereis
 */


/*
 * Import Scopes
 */


/*
 * Convert Arrays
 */


/*
 * Calling Super
 */


/*
 * Loading Scripts
 */