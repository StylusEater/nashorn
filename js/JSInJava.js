/* 
 * Java Nashorn examples taken from:
 * 
 *     http://winterbe.com/posts/2014/04/05/java8-nashorn-tutorial/
 *     
 */

/* Example call from FileReader ... */
print('Hello, world!');

/* Add some functions we'll call from Java land */
var fun1 = function(name) {
    print('Hi there from Javascript, ' + name);
    return "greetings from javascript";
};

var fun2 = function (object) {
    print("JS Class Definition: " + Object.prototype.toString.call(object));
};



