/*
    Nashorn Example Code
    Copyright (C) 2015  Adam M Dutko

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.nashorn;

import java.util.Arrays;

import jdk.nashorn.api.scripting.ScriptObjectMirror;

/**
 *
 * @author Adam M Dutko
 * @version 0.01
 * 
 * A Java class where we write Java to use in JavaScript files.
 *  
 */
public class JavaInJS {
    
    /*
    * Example function to be called via Javascript.
    * We declare it static so we don't have to instantiate.
    *
    * @param name a person's name
    */
    public static String printName(String name) {
        System.out.println("Hi there from Java!");
        return name + ", greetings from java!";
    }
    
    
    /*
    * Example function to be called via Javascript.
    * We declare it static so we don't have to instantiate.
    *
    * @param any valid JavaScript type as a parameter.
    */
    public static void printClassTypeOfMethodParameter(Object object) {
        System.out.println(object.getClass());
    }
    
    
    /* Use the ScriptObjectMirror class that gives us a Java representation of the 
    * JavaScript object.
    * 
    */
    public static void mirrorObject(ScriptObjectMirror mirror) {
        System.out.println(mirror.getClassName() + ": " + 
            Arrays.toString(mirror.getOwnKeys(true)));
    }
    
    
    // Call member function for a particular object.
    public static void printFullName(ScriptObjectMirror person) {
        System.out.println("Full name is: " + person.callMember("printFullName"));
    }

    
}
