/*
    Nashorn Example Code
    Copyright (C) 2015  Adam M Dutko

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.nashorn;

// All of the items we need to run JavaScript in Java
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.script.Invocable;

// Various required Java classes
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.Properties;

/**
 *
 * @author Adam M Dutko
 * @version 0.01
 * 
 * A Java class where we write Javascript and use Javascript in Java.
 * 
 */
public class JSInJava {

    /**
     * 
     * @param args the command line arguments
     * @throws IOException if nashorn.properties not found
     * 
     * Main method where we call our examples.
     */
    public static void main(String[] args) throws IOException {
        
        
        // Create an instance of the nashorn engine
        ScriptEngine engine = new ScriptEngineManager().getEngineByName("nashorn");
        
        // Get our properties from nashorn.properties
        Properties props = new Properties();
        InputStream propsFile = new FileInputStream("nashorn.properties");
        props.load(propsFile);
        
        try {
            // Print text using an eval statement ... eval is supposed to be 
            // avoided at all costs per Attila ... the a Nashorn Architect
            engine.eval("print('Hello World!');");
            
            // Read in the contents of an ECMA 5.1 compliant javascript source 
            // file and execute it
            System.out.println(props.getProperty("js.in.java"));
            engine.eval(new FileReader(props.getProperty("js.in.java")));
            
            // Allow us to call javascript functions "natively"
            Invocable invocable = (Invocable) engine;
            
            // Call fun1 function written in javascript from Java
            Object result = invocable.invokeFunction("fun1", "Peter Parker");
            System.out.println(result);
            System.out.println(result.getClass());
            
            // Call fun2 function written in javascript from Java
            Object result2 = invocable.invokeFunction("fun2", new Date());
            System.out.println(result2);
            
        } catch(ScriptException | FileNotFoundException | NoSuchMethodException e)
        {
            // Catch various throwable exceptions and print the value
            System.out.println(e.toString());
        }
    }
    
}
