# [Project Nashorn](http://openjdk.java.net/projects/nashorn/)

Nashorn's goal is to implement a lightweight high-performance JavaScript 
runtime in Java with a native JVM. This Project intends to enable Java 
developers embedding of JavaScript in Java applications via [JSR-223](http://jcp.org/en/jsr/detail?id=223) and 
to develop free standing JavaScript applications using the [jrunscript](http://docs.oracle.com/javase/7/docs/technotes/tools/share/jrunscript.html) 
command-line tool. 

## Examples

This project provides working example code adapted from the [Java Nashorn 
tutorial](http://winterbe.com/posts/2014/04/05/java8-nashorn-tutorial/).

There is even more code located on [github](https://github.com/winterbe/java8-tutorial).

## Usage

This project should be imported as a Netbeans project. 

The JSInJava portion of the project should be run using the tasks provided 
through the build.xml file. Before attempting to use the `run` task you will 
need to create a nashorn.properties file containing the absolute paths of the 
JavaScript files located under the `js/` directory.

The JavaInJS portion of the project should be run using [`jjs`](http://docs.oracle.com/javase/8/docs/technotes/tools/unix/jjs.html) after compiling
the required classes and building `nashorn.jar`. Here is an example invocation
using [`jjs`](http://docs.oracle.com/javase/8/docs/technotes/tools/unix/jjs.html):

> jjs -cp dist/nashorn.jar js/JavaInJS.js


## Links
[Nashorn's User Guide](http://docs.oracle.com/javase/8/docs/technotes/guides/scripting/nashorn/)

[Introducting Nashorn](http://www.oracle.com/technetwork/articles/java/jf14-nashorn-2126515.html)

[Nashorn Extensions](https://wiki.openjdk.java.net/display/Nashorn/Nashorn+extensions)

[Shell Scripts with Nashorn](http://docs.oracle.com/javase/8/docs/technotes/guides/scripting/nashorn/shell.html#sthref24)

[Using Backbone.js with Nashorn](http://winterbe.com/posts/2014/04/07/using-backbonejs-with-nashorn/)